<?php
    $myFile = "config.txt";
    $fh = fopen($myFile, 'r');
    $theData = fread($fh, filesize($myFile));
    $config = array();
    $config = json_decode($theData, true);
?>

<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Megaw &#8211; Multi Purpose HTML Template</title>
    <link rel='stylesheet' id='layerslider-css'  href='css/layerslider.css?ver=5.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='ls-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css'  href='plugins/revslider/public/assets/css/settings.css?ver=5.0.9' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-layout-css'  href='plugins/woocommerce/assets/css/woocommerce-layout.css?ver=2.5.2' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-smallscreen-css'  href='plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=2.5.2' type='text/css' media='only screen and (max-width: 768px)' />
    <link rel='stylesheet' id='woocommerce-general-css'  href='plugins/woocommerce/assets/css/woocommerce.css?ver=2.5.2' type='text/css' media='all' />
    <link rel='stylesheet' id='megamenu-css'  href='maxmegamenu/style.css?ver=762094' type='text/css' media='all' />
    <link rel='stylesheet' id='dashicons-css'  href='css/dashicons.min.css?ver=4.4.2' type='text/css' media='all' />
    <link rel='stylesheet' id='yith-quick-view-css'  href='plugins/yith-woocommerce-quick-view/assets/css/yith-quick-view.css?ver=4.4.2' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'  href='plugins/woocommerce/assets/css/prettyPhoto.css?ver=4.4.2' type='text/css' media='all' />
    <link rel='stylesheet' id='jquery-selectBox-css'  href='plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css?ver=4.4.2' type='text/css' media='all' />
    <link rel='stylesheet' id='yith-wcwl-main-css'  href='plugins/yith-woocommerce-wishlist/assets/css/style.css?ver=4.4.2' type='text/css' media='all' />
    <link rel='stylesheet' id='yith-wcwl-font-awesome-css'  href='plugins/yith-woocommerce-wishlist/assets/css/font-awesome.min.css?ver=4.4.2' type='text/css' media='all' />
    <link rel='stylesheet' id='mailchimp-for-wp-checkbox-css'  href='plugins/mailchimp-for-wp/assets/css/checkbox.min.css?ver=2.3.7' type='text/css' media='all' />
    <link rel='stylesheet' id='wope-gfont-Montserrat-css'  href='http://fonts.googleapis.com/css?family=Montserrat%3A400%2C700&#038;ver=4.4.2' type='text/css' media='all' />
    <link rel='stylesheet' id='wope-gfont-Open-Sans-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C400%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&#038;ver=4.4.2' type='text/css' media='all' />
    <link rel='stylesheet' id='wope-default-style-css'  href='style.css?ver=4.4.2' type='text/css' media='all' />
    <link rel='stylesheet' id='wope-responsive-css'  href='responsive.css?ver=4.4.2' type='text/css' media='all' />
    <link rel='stylesheet' id='flexslider-style-css'  href='js/flex-slider/flexslider.css?ver=4.4.2' type='text/css' media='all' />
    <link rel='stylesheet' id='magnific-popup-style-css'  href='js/magnific-popup/magnific-popup.css?ver=4.4.2' type='text/css' media='all' />
    <link rel='stylesheet' id='wope-font-awesome-css'  href='font-awesome/css/font-awesome.min.css?ver=4.4.2' type='text/css' media='all' />
    <link rel='stylesheet' id='wope-font-pe-icon-7-stroke-css'  href='pe-icon-7-stroke/css/pe-icon-7-stroke.css?ver=4.4.2' type='text/css' media='all' />
    <script src='plugins/LayerSlider/static/js/greensock.js?ver=1.11.8'></script>
    <script src='js/jquery/jquery.js?ver=3.0.0'></script>
    <script src='js/jquery/jquery-migrate.min.js?ver=3.0.1'></script>
    <script src='plugins/LayerSlider/static/js/layerslider.kreaturamedia.jquery.js?ver=5.3.2'></script>
    <script src='plugins/LayerSlider/static/js/layerslider.transitions.js?ver=5.3.2'></script>
    <script src='plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.0.9'></script>
    <script src='plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.0.9'></script>
    <script src='js/hoverIntent.min.js?ver=1.8.1'></script>
    <script src='plugins/megamenu/js/maxmegamenu.js?ver=1.8.2'></script>
    <link rel='stylesheet' href='css/custom.css' type='text/css' media='all' />
    <script>
        $(document).ready(function(){
            $('a[href^="#"]').on('click',function (e) {
                e.preventDefault();
                var target = this.hash;
                $target = $(target);
                $('html, body').stop().animate({
                    'scrollTop':  $target.offset().top //no need of parseInt here
                }, 900, 'swing', function () {
                    window.location.hash = target;
                });
            });
        });
        $(document).on('ready', function()   {
            $('#scroll-header').slideUp();
        });

        $(window).scroll(function() {
            if ($(this).scrollTop() > 1){
                $('#scroll-header').slideDown();
            }
            else{
                $('#scroll-header').slideUp();
            }
        });
    </script>
</head>
<body class="home page page-template-default">
<div id="background" style="">
    <div id="scroll-header">
        <div id="scroll-header-content">
            <div class="wrap">
                <div class="left-header">
                    <div class="site-logo"> <a class="logo-image" href=""> <img class="logo-normal" alt="Megaw" src="images/scroll-logo.png"> <img class="logo-retina" alt="Megaw" src="images/scroll-logo-retina.png"> </a> </div>
                </div>
                <div class="right-header">
                    <div class="main-menu">
                        <div id="mega-menu-wrap-main-menu" class="mega-menu-wrap">
                            <div class="mega-menu-toggle"></div>
                            <ul id="mega-menu-main-menu" class="mega-menu mega-menu-horizontal mega-no-js" data-event="hover" data-effect="disabled" data-second-click="close" data-breakpoint="600">
                                <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-current-menu-item mega-current_page_item mega-menu-item-home mega-current-menu-ancestor mega-current-menu-parent mega-menu-item-has-children mega-menu-item-18 mega-align-bottom-left mega-menu-flyout'><a href="#page">Home</a></li>
                                <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-menu-item-19 mega-align-bottom-left mega-menu-flyout'><a href="#about-us-<?php echo $config['about-us']; ?>">About Us</a></li>
                                <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-menu-item-20 mega-align-bottom-left mega-menu-flyout' ><a href="#services-<?php echo $config['services']; ?>">Services</a></li>
                                <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-menu-item-21 mega-align-bottom-left mega-menu-megamenu'><a href="#gallery-<?php echo $config['gallery']; ?>">Gallery</a></li>
                                <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-menu-item-22 mega-align-bottom-left mega-menu-flyout'><a href="#testimonials-<?php echo $config['testimonials']; ?>">Testimonials</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Main Menu -->
                </div>
                <div class="cleared"></div>
            </div>
        </div>
    </div>
    <div id="page" >
        <div id="header" class="header-transparent header-text-white header-on-slider" style="">
            <div class='header_bg' style=""></div>
            <div class="header_content">
                <div id="top-bar-open">
                    <div class="wrap">
                        <div id="topbar-open"><i class="fa fa-angle-down"></i></div>
                    </div>
                </div>
                <div class="wrap">
                    <div class="left-header">
                        <div class="site-logo">
                            <h1> <a class="logo-image" href=""> <img class="logo-normal" alt="Megaw" src="images/logo-white.png" /> <img class="logo-retina"   alt="Megaw" src="images/logo-white-retina.png" /> </a> </h1>
                        </div>
                    </div>
                    <div class="right-header">
                        <div class="main-menu">
                            <div id="mega-menu-wrap-main-menu" class="mega-menu-wrap">
                                <div class="mega-menu-toggle"></div>
                                <ul id="mega-menu-main-menu" class="mega-menu mega-menu-horizontal mega-no-js" data-event="hover" data-effect="disabled" data-second-click="close" data-breakpoint="600">
                                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-current-menu-item mega-current_page_item mega-menu-item-home mega-current-menu-ancestor mega-current-menu-parent mega-menu-item-has-children mega-menu-item-18 mega-align-bottom-left mega-menu-flyout'><a href="#page">Home</a></li>
                                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-menu-item-19 mega-align-bottom-left mega-menu-flyout'><a href="#about-us-<?php echo $config['about-us']; ?>">About Us</a></li>
                                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-menu-item-20 mega-align-bottom-left mega-menu-flyout' ><a href="#services-<?php echo $config['services']; ?>">Services</a></li>
                                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-menu-item-21 mega-align-bottom-left mega-menu-megamenu'><a href="#gallery-<?php echo $config['gallery']; ?>">Gallery</a></li>
                                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-menu-item-22 mega-align-bottom-left mega-menu-flyout'><a href="#testimonials-<?php echo $config['testimonials']; ?>">Testimonials</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Main Menu -->
                    </div>
                    <div class="cleared"></div>
                    <div id="toggle-menu-button"><i class="fa fa-align-justify"></i></div>
                    <div class="toggle-menu">
                        <div class="menu-toggle-menu-container">
                            <ul id="menu-toggle-menu" class="menu">
                                <li  class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-1235 current_page_item menu-item-1264"><a href="">Home</a></li>
                                <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1266"><a href="about-us.html">About Classic</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1267"><a href="portfolio-grid.html">Portfolio</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1268"><a href="blog-standard.html">Blog</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1265"><a href="contact.html">Contact Modern</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Header Wrap -->
                <!-- End Header -->
            </div>
            <!-- End Header Content -->
        </div>
        <!-- end Header -->
        <div class="cleared"></div>
        <!-- BEGIN CONTENT -->
        <div id="body">
            <div id="content-section1" class="content-section black-text white-bg top-spacing-no bottom-spacing-no title-spacing-medium align-center " style="">
                <div class="no-wrap">
                    <div class="content-column1">
                        <div class="widget-entry">
                            <div class="column1">
                                <div class="content content-box-content">
                                    <div id="rev_slider_3_1_wrapper" class="rev_slider_wrapper fullscreen-container" style="background-color:#6b6b6b;padding:0px;">
                                        <!-- START REVOLUTION SLIDER 5.0.9 fullscreen mode -->
                                        <div id="rev_slider_3_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.0.9">
                                            <ul>
                                                <!-- SLIDE  -->
                                                <li data-index="rs-28" data-transition="fade" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="images/1_low-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-description="">
                                                    <!-- MAIN IMAGE -->
                                                    <img src="images/dummy.png"  alt=""  width="1400" height="882" data-lazyload="images/1_low.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                                                    <!-- LAYERS -->

                                                    <!-- LAYER NR. 1 -->
                                                    <div class="tp-caption Megaw-Headline1   tp-resizeme rs-parallaxlevel-0"
                                                         id="slide-28-layer-1"
                                                         data-x="['center','center','center','center']" data-hoffset="['7','7','7','3']"
                                                         data-y="['top','top','top','top']" data-voffset="['203','203','203','159']"
                                                         data-fontsize="['60','60','40','24']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"
                                                         data-transform_idle="o:1;"
                                                         data-transform_in="x:-50px;opacity:0;s:300;e:Power2.easeInOut;"
                                                         data-transform_out="opacity:0;s:300;s:300;"
                                                         data-start="500"
                                                         data-splitin="none"
                                                         data-splitout="none"
                                                         data-responsive_offset="on"
                                                         style="z-index: 5; white-space: nowrap;">PASSIONATE & CREATIVE
                                                    </div>
                                                    <!-- LAYER NR. 2 -->
                                                    <div class="tp-caption Megaw-Text   tp-resizeme rs-parallaxlevel-0"
                                                         id="slide-28-layer-2"
                                                         data-x="['center','center','center','center']" data-hoffset="['6','6','6','8']"
                                                         data-y="['top','top','top','top']" data-voffset="['310','310','288','224']"
                                                         data-fontsize="['30','30','24','18']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"
                                                         data-transform_idle="o:1;"
                                                         data-transform_in="x:50px;opacity:0;s:300;e:Power2.easeInOut;"
                                                         data-transform_out="opacity:0;s:300;s:300;"
                                                         data-start="1000"
                                                         data-splitin="none"
                                                         data-splitout="none"
                                                         data-responsive_offset="on"
                                                         style="z-index: 6; white-space: nowrap;letter-spacing:0px;">everything you need to build an awesome website
                                                    </div>
                                                    <!-- LAYER NR. 3 -->
                                                    <div class="tp-caption Megaw-Button rev-btn  rs-parallaxlevel-0"
                                                         id="slide-28-layer-3"
                                                         data-x="['center','center','center','center']" data-hoffset="['9','9','10','10']"
                                                         data-y="['top','top','top','top']" data-voffset="['398','398','366','289']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"
                                                         data-transform_idle="o:1;"
                                                         data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:200;e:Power0.easeOut;"
                                                         data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);background-color:#ffffff;cursor:pointer;"
                                                         data-transform_in="y:50px;opacity:0;s:300;e:Power2.easeInOut;"
                                                         data-transform_out="opacity:0;s:300;s:300;"
                                                         data-start="1500"
                                                         data-splitin="none"
                                                         data-splitout="none"
                                                         data-responsive_offset="on"
                                                         data-responsive="off"
                                                         style="z-index: 7; white-space: nowrap;">LEARN MORE
                                                    </div>
                                                </li>
                                                <!-- SLIDE  -->
                                                <li data-index="rs-29" data-transition="fade" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="images/2_low-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-description="">
                                                    <!-- MAIN IMAGE -->
                                                    <img src="images/dummy.png"  alt=""  width="1400" height="882" data-lazyload="images/2_low.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                                                    <!-- LAYERS -->

                                                    <!-- LAYER NR. 1 -->
                                                    <div class="tp-caption Megaw-Headline1   tp-resizeme rs-parallaxlevel-0"
                                                         id="slide-29-layer-1"
                                                         data-x="['center','center','center','center']" data-hoffset="['7','7','7','3']"
                                                         data-y="['top','top','top','top']" data-voffset="['203','203','203','159']"
                                                         data-fontsize="['60','60','40','24']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"
                                                         data-transform_idle="o:1;"
                                                         data-transform_in="x:-50px;opacity:0;s:300;e:Power2.easeInOut;"
                                                         data-transform_out="opacity:0;s:300;s:300;"
                                                         data-start="500"
                                                         data-splitin="none"
                                                         data-splitout="none"
                                                         data-responsive_offset="on"
                                                         style="z-index: 5; white-space: nowrap;">ENJOY THE DIFFERENCE
                                                    </div>
                                                    <!-- LAYER NR. 2 -->
                                                    <div class="tp-caption Megaw-Text   tp-resizeme rs-parallaxlevel-0"
                                                         id="slide-29-layer-2"
                                                         data-x="['center','center','center','center']" data-hoffset="['6','6','6','8']"
                                                         data-y="['top','top','top','top']" data-voffset="['310','310','288','224']"
                                                         data-fontsize="['30','30','24','18']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"
                                                         data-transform_idle="o:1;"
                                                         data-transform_in="x:50px;opacity:0;s:300;e:Power2.easeInOut;"
                                                         data-transform_out="opacity:0;s:300;s:300;"
                                                         data-start="1000"
                                                         data-splitin="none"
                                                         data-splitout="none"
                                                         data-responsive_offset="on"
                                                         style="z-index: 6; white-space: nowrap;letter-spacing:0px;">let start building your idea to life
                                                    </div>
                                                    <!-- LAYER NR. 3 -->
                                                    <div class="tp-caption Megaw-Button rev-btn  rs-parallaxlevel-0"
                                                         id="slide-29-layer-3"
                                                         data-x="['center','center','center','center']" data-hoffset="['9','9','10','10']"
                                                         data-y="['top','top','top','top']" data-voffset="['398','398','366','289']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"
                                                         data-transform_idle="o:1;"
                                                         data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:200;e:Power0.easeOut;"
                                                         data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);background-color:#ffffff;cursor:pointer;"
                                                         data-transform_in="y:50px;opacity:0;s:300;e:Power2.easeInOut;"
                                                         data-transform_out="opacity:0;s:300;s:300;"
                                                         data-start="1500"
                                                         data-splitin="none"
                                                         data-splitout="none"
                                                         data-responsive_offset="on"
                                                         data-responsive="off"
                                                         style="z-index: 7; white-space: nowrap;">START HERE
                                                    </div>
                                                </li>
                                                <!-- SLIDE  -->
                                                <li data-index="rs-30" data-transition="fade" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="images/3_low-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-description="">
                                                    <!-- MAIN IMAGE -->
                                                    <img src="images/dummy.png"  alt=""  width="1400" height="882" data-lazyload="images/3_low.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                                                    <!-- LAYERS -->

                                                    <!-- LAYER NR. 1 -->
                                                    <div class="tp-caption Megaw-Headline1   tp-resizeme rs-parallaxlevel-0"
                                                         id="slide-30-layer-1"
                                                         data-x="['center','center','center','center']" data-hoffset="['7','7','7','3']"
                                                         data-y="['top','top','top','top']" data-voffset="['203','203','203','159']"
                                                         data-fontsize="['60','55','40','24']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"
                                                         data-transform_idle="o:1;"

                                                         data-transform_in="x:-50px;opacity:0;s:300;e:Power2.easeInOut;"
                                                         data-transform_out="opacity:0;s:300;s:300;"
                                                         data-start="500"
                                                         data-splitin="none"
                                                         data-splitout="none"
                                                         data-responsive_offset="on"
                                                         style="z-index: 5; white-space: nowrap;">YOU DESERVE A BEAUTIFUL LIFE
                                                    </div>
                                                    <!-- LAYER NR. 2 -->
                                                    <div class="tp-caption Megaw-Text   tp-resizeme rs-parallaxlevel-0"
                                                         id="slide-30-layer-2"
                                                         data-x="['center','center','center','center']" data-hoffset="['6','6','6','8']"
                                                         data-y="['top','top','top','top']" data-voffset="['310','310','288','224']"
                                                         data-fontsize="['30','30','24','18']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"
                                                         data-transform_idle="o:1;"

                                                         data-transform_in="x:50px;opacity:0;s:300;e:Power2.easeInOut;"
                                                         data-transform_out="opacity:0;s:300;s:300;"
                                                         data-start="1000"
                                                         data-splitin="none"
                                                         data-splitout="none"
                                                         data-responsive_offset="on"
                                                         style="z-index: 6; white-space: nowrap;letter-spacing:0px;"> mega is sharp , modern & clean wordpress theme
                                                    </div>
                                                    <!-- LAYER NR. 3 -->
                                                    <div class="tp-caption Megaw-Button rev-btn  rs-parallaxlevel-0"
                                                         id="slide-30-layer-3"
                                                         data-x="['center','center','center','center']" data-hoffset="['9','9','10','10']"
                                                         data-y="['top','top','top','top']" data-voffset="['398','398','366','289']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"
                                                         data-transform_idle="o:1;"
                                                         data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:200;e:Power0.easeOut;"
                                                         data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);background-color:#ffffff;cursor:pointer;"
                                                         data-transform_in="y:50px;opacity:0;s:300;e:Power2.easeInOut;"
                                                         data-transform_out="opacity:0;s:300;s:300;"
                                                         data-start="1500"
                                                         data-splitin="none"
                                                         data-splitout="none"
                                                         data-responsive_offset="on"
                                                         data-responsive="off"
                                                         style="z-index: 7; white-space: nowrap;">TRY IT
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                                        </div>
                                        <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss=".tp-caption.Megaw-Headline1,.Megaw-Headline1{color:rgba(255,255,255,1.00);font-size:60px;line-height:60px;font-weight:700;font-style:normal;font-family:Montserrat;padding:0px 0px 0px 0px;text-decoration:none;text-align:left;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px}.tp-caption.Megaw-Text,.Megaw-Text{color:rgba(255,255,255,1.00);font-size:30px;line-height:36px;font-weight:300;font-style:normal;font-family:Open Sans;padding:0px 0px 0px 0px;text-decoration:none;text-align:left;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px}.tp-caption.Megaw-Button,.Megaw-Button{color:rgba(255,255,255,1.00);font-size:18px;line-height:18px;font-weight:400;font-style:normal;font-family:Montserrat;padding:13px 35px 13px 35px;text-decoration:none;text-align:left;background-color:transparent;border-color:rgba(255,255,255,1.00);border-style:solid;border-width:2px;border-radius:0px 0px 0px 0px}.tp-caption.Megaw-Button:hover,.Megaw-Button:hover{color:rgba(46,46,46,1.00);text-decoration:none;background-color:rgba(255,255,255,1.00);border-color:rgba(255,255,255,1.00);border-style:solid;border-width:2px;border-radius:0px 0px 0px 0px}";
                                        if(htmlDiv) {
                                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                        }
                                        else{
                                            var htmlDiv = document.createElement("div");
                                            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                        }
                                        </script>
                                        <script>
                                            /******************************************
                                             -	PREPARE PLACEHOLDER FOR SLIDER	-
                                             ******************************************/

                                            var setREVStartSize=function(){
                                                try{var e=new Object,i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
                                                    e.c = jQuery('#rev_slider_3_1');
                                                    e.responsiveLevels = [1240,1024,778,480];
                                                    e.gridwidth = [1240,1024,778,480];
                                                    e.gridheight = [600,600,500,400];

                                                    e.sliderLayout = "fullscreen";
                                                    e.fullScreenAutoWidth='off';
                                                    e.fullScreenAlignForce='off';
                                                    e.fullScreenOffsetContainer= '';
                                                    e.fullScreenOffset='';
                                                    if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
                                                }catch(d){console.log("Failure at Presize of Slider:"+d)}
                                            };


                                            setREVStartSize();
                                            function revslider_showDoubleJqueryError(sliderID) {
                                                var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
                                                errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
                                                errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
                                                errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
                                                errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>"
                                                jQuery(sliderID).show().html(errorMessage);
                                            }
                                            var tpj=jQuery;

                                            var revapi3;
                                            tpj(document).ready(function() {
                                                if(tpj("#rev_slider_3_1").revolution == undefined){
                                                    revslider_showDoubleJqueryError("#rev_slider_3_1");
                                                }else{
                                                    revapi3 = tpj("#rev_slider_3_1").show().revolution({
                                                        sliderType:"standard",
                                                        jsFileLocation:"plugins/revslider/public/assets/js/",
                                                        sliderLayout:"fullscreen",
                                                        dottedOverlay:"none",
                                                        delay:9000,
                                                        navigation: {
                                                            keyboardNavigation:"off",
                                                            keyboard_direction: "horizontal",
                                                            mouseScrollNavigation:"off",
                                                            onHoverStop:"on",
                                                            touch:{
                                                                touchenabled:"on",
                                                                swipe_threshold: 75,
                                                                swipe_min_touches: 50,
                                                                swipe_direction: "horizontal",
                                                                drag_block_vertical: false
                                                            }
                                                            ,
                                                            arrows: {
                                                                style:"zeus",
                                                                enable:true,
                                                                hide_onmobile:true,
                                                                hide_under:600,
                                                                hide_onleave:true,
                                                                hide_delay:200,
                                                                hide_delay_mobile:1200,
                                                                tmp:'<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div> </div>',
                                                                left: {
                                                                    h_align:"left",
                                                                    v_align:"center",
                                                                    h_offset:30,
                                                                    v_offset:0
                                                                },
                                                                right: {
                                                                    h_align:"right",
                                                                    v_align:"center",
                                                                    h_offset:30,
                                                                    v_offset:0
                                                                }
                                                            }
                                                        },
                                                        responsiveLevels:[1240,1024,778,480],
                                                        gridwidth:[1240,1024,778,480],
                                                        gridheight:[600,600,500,400],
                                                        lazyType:"smart",
                                                        parallax: {
                                                            type:"mouse",
                                                            origo:"slidercenter",
                                                            speed:2000,
                                                            levels:[2,3,4,5,6,7,12,16,10,50],
                                                        },
                                                        shadow:0,
                                                        spinner:"off",
                                                        stopLoop:"off",
                                                        stopAfterLoops:-1,
                                                        stopAtSlide:-1,
                                                        shuffle:"off",
                                                        autoHeight:"off",
                                                        fullScreenAutoWidth:"off",
                                                        fullScreenAlignForce:"off",
                                                        fullScreenOffsetContainer: "",
                                                        fullScreenOffset: "",
                                                        disableProgressBar:"on",
                                                        hideThumbsOnMobile:"off",
                                                        hideSliderAtLimit:0,
                                                        hideCaptionAtLimit:0,
                                                        hideAllCaptionAtLilmit:0,
                                                        debugMode:false,
                                                        fallbacks: {
                                                            simplifyAll:"off",
                                                            nextSlideOnWindowFocus:"off",
                                                            disableFocusListener:false,
                                                        }
                                                    });
                                                }
                                            });	/*ready*/
                                        </script>
                                        <script>
                                            var htmlDivCss = unescape(".zeus.tparrows%20%7B%0A%20%20cursor%3Apointer%3B%0A%20%20min-width%3A70px%3B%0A%20%20min-height%3A70px%3B%0A%20%20position%3Aabsolute%3B%0A%20%20display%3Ablock%3B%0A%20%20z-index%3A100%3B%0A%20%20border-radius%3A35px%3B%20%20%20%0A%20%20overflow%3Ahidden%3B%0A%20%20background%3Argba%280%2C0%2C0%2C0.10%29%3B%0A%7D%0A%0A.zeus.tparrows%3Abefore%20%7B%0A%20%20font-family%3A%20%22revicons%22%3B%0A%20%20font-size%3A20px%3B%0A%20%20color%3A%23fff%3B%0A%20%20display%3Ablock%3B%0A%20%20line-height%3A%2070px%3B%0A%20%20text-align%3A%20center%3B%20%20%20%20%0A%20%20z-index%3A2%3B%0A%20%20position%3Arelative%3B%0A%7D%0A.zeus.tparrows.tp-leftarrow%3Abefore%20%7B%0A%20%20content%3A%20%22%5Ce824%22%3B%0A%7D%0A.zeus.tparrows.tp-rightarrow%3Abefore%20%7B%0A%20%20content%3A%20%22%5Ce825%22%3B%0A%7D%0A%0A.zeus%20.tp-title-wrap%20%7B%0A%20%20background%3A%23000%3B%0A%20%20background%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%20%20width%3A100%25%3B%0A%20%20height%3A100%25%3B%0A%20%20top%3A0px%3B%0A%20%20left%3A0px%3B%0A%20%20position%3Aabsolute%3B%0A%20%20opacity%3A0%3B%0A%20%20transform%3Ascale%280%29%3B%0A%20%20-webkit-transform%3Ascale%280%29%3B%0A%20%20%20transition%3A%20all%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%20-moz-transition%3Aall%200.3s%3B%0A%20%20%20border-radius%3A50%25%3B%0A%20%7D%0A.zeus%20.tp-arr-imgholder%20%7B%0A%20%20width%3A100%25%3B%0A%20%20height%3A100%25%3B%0A%20%20position%3Aabsolute%3B%0A%20%20top%3A0px%3B%0A%20%20left%3A0px%3B%0A%20%20background-position%3Acenter%20center%3B%0A%20%20background-size%3Acover%3B%0A%20%20border-radius%3A50%25%3B%0A%20%20transform%3AtranslateX%28-100%25%29%3B%0A%20%20-webkit-transform%3AtranslateX%28-100%25%29%3B%0A%20%20%20transition%3A%20all%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%20-moz-transition%3Aall%200.3s%3B%0A%0A%20%7D%0A.zeus.tp-rightarrow%20.tp-arr-imgholder%20%7B%0A%20%20%20%20transform%3AtranslateX%28100%25%29%3B%0A%20%20-webkit-transform%3AtranslateX%28100%25%29%3B%0A%20%20%20%20%20%20%7D%0A.zeus.tparrows%3Ahover%20.tp-arr-imgholder%20%7B%0A%20%20transform%3AtranslateX%280%29%3B%0A%20%20-webkit-transform%3AtranslateX%280%29%3B%0A%20%20opacity%3A1%3B%0A%7D%0A%20%20%20%20%20%20%0A.zeus.tparrows%3Ahover%20.tp-title-wrap%20%7B%0A%20%20transform%3Ascale%281%29%3B%0A%20%20-webkit-transform%3Ascale%281%29%3B%0A%20%20opacity%3A1%3B%0A%7D%0A%20%0A");
                                            var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                            if(htmlDiv) {
                                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                            }
                                            else{
                                                var htmlDiv = document.createElement('div');
                                                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                            }
                                        </script>
                                    </div>
                                    <!-- END REVOLUTION SLIDER -->
                                </div>
                            </div>
                            <div class="cleared"></div>
                            <div class="cleared"></div>
                        </div>
                        <!-- end widget entry --> </div>
                    <div class="cleared"></div>
                </div>
                <!-- end wrap-column no-wrap -->

            </div>
            <!-- end content section -->

            <?php if ($config['about-us'] == 1) { ?>
            <div id="about-us-1" class="content-section black-text white-bg top-spacing-medium bottom-spacing-no title-spacing-big align-center " style="">
                <div class="wrap-column">
                    <div class="column1">
                        <div class="wrap">
                            <div class="content-section-heading">
                                <h1 class="content-section-title"> ABOUT US 1</h1>
                                <div class="content-section-text">
                                    <p>We is accepted as one of the finest in the digital marketing industry. Since 2006, we have powered </p>
                                    <p>with thousand of clients, make their brands successed online.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cleared"></div>
                </div>
                <div class="wrap-column">
                    <div class="content-column1"> </div>
                    <div class="cleared"></div>
                </div>
                <!-- end wrap-column no-wrap -->
            </div>
            <!-- end content section -->
            <?php } ?>

            <?php if ($config['about-us'] == 2) { ?>
                <div id="about-us-2" class="content-section black-text gray-bg top-spacing-no bottom-spacing-no title-spacing-medium align-center " style="padding: 50px 0 0 0">
                    <div class="wrap-column">
                        <div class="column1">
                            <div class="wrap">
                                <div class="content-section-heading">
                                    <h1 class="content-section-title"> ABOUT US 2 </h1>
                                </div>
                            </div>
                        </div>
                        <div class="cleared"></div>
                    </div>
                    <div class="section-half-column">
                        <div class="section-column-content">
                            <div class="content-section-text">
                                <p>We is accepted as one of the finest in the digital marketing industry. Since 2006, we have powered </p>
                                <p>with thousand of clients, make their brands successed online.</p>
                            </div>
                        </div>
                        <div class="section-column-bg" style="background-image: url('images/photodune-11523090-discussing-sketch-m.jpg');"></div>
                    </div>
                </div>
                <!-- end content section -->
            <?php } ?>

            <?php if ($config['about-us'] == 3) { ?>
                <div id="about-us-3" class="content-section black-text gray-bg top-spacing-no bottom-spacing-no title-spacing-medium align-center " style="padding: 50px 0 0 0">
                    <div class="wrap-column">
                        <div class="column1">
                            <div class="wrap">
                                <div class="content-section-heading">
                                    <h1 class="content-section-title"> ABOUT US 3 </h1>
                                </div>
                            </div>
                        </div>
                        <div class="cleared"></div>
                    </div>
                    <div class="section-half-column">
                        <div class="section-column-bg" style="background-image: url('images/photodune-11523090-discussing-sketch-m.jpg');"></div>
                        <div class="section-column-content">
                            <div class="content-section-text">
                                <p>We is accepted as one of the finest in the digital marketing industry. Since 2006, we have powered </p>
                                <p>with thousand of clients, make their brands successed online.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end content section -->
            <?php } ?>

            <?php if ($config['about-us'] == 4) { ?>
                <div id="about-us-4" class="content-section black-text white-bg top-spacing-medium bottom-spacing-no title-spacing-medium align-left " style="margin-bottom: 20px;">
                    <div class="wrap-column">
                        <div class="column1">
                            <div class="wrap">
                                <div class="content-section-heading">
                                    <h1 class="content-section-title"> ABOUT US 4 </h1>
                                </div>
                            </div>
                        </div>
                        <div class="cleared"></div>
                    </div>
                    <div class="wrap-column">
                        <div class="content-column2_1">
                            <div class="widget-entry">
                                <div class="column1">
                                    <div class="gallery ">
                                        <div class="percent_column1 column-last">
                                            <div class="gallery-grid"> <img title="" src="images/team3.jpg"> </div>
                                        </div>
                                        <div class="cleared"></div>
                                    </div>
                                </div>
                                <div class="cleared"></div>
                                <div class="cleared"></div>
                            </div>
                            <!-- end widget entry --> </div>
                        <div class="content-column2_1">
                            <div class="widget-entry">
                                <div class="column1">
                                    <div class="content content-box-content">
                                        <p>INNOVATE DIGITAL DESIGNER</p>
                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet</p>
                                        <p>Lorem ipsum dolor sit amet, ex pro purto dolores, eu ius commodo nostrum. Cu eos augue feugait. Vidisse definitiones in pri. Praesent augue arcu, ornare ut tincidunt eu, mattis a libero. Pellentesque habitant morbi tristique senectus et netus et malesuada </p>
                                    </div>
                                </div>
                                <div class="cleared"></div>
                                <div class="cleared"></div>
                            </div>
                            <!-- end widget entry --> </div>
                        <div class="cleared"></div>
                    </div>
                    <!-- end wrap-column no-wrap -->

                </div>
                <!-- end content section -->
            <?php } ?>

            <?php if ($config['about-us'] == 5) { ?>
                <div id="about-us-4" class="content-section black-text white-bg top-spacing-medium bottom-spacing-no title-spacing-medium align-left " style="margin-bottom: 20px;">
                        <div class="wrap-column">
                            <div class="column1">
                                <div class="wrap">
                                    <div class="content-section-heading">
                                        <h1 class="content-section-title"> ABOUT US 5 </h1>
                                    </div>
                                </div>
                            </div>
                            <div class="cleared"></div>
                        </div>
                    <div class="wrap-column">
                        <div class="content-column2_1">
                            <div class="widget-entry">
                                <div class="column1">
                                    <div class="content content-box-content">
                                        <p>INNOVATE DIGITAL DESIGNER</p>
                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet</p>
                                        <p>Lorem ipsum dolor sit amet, ex pro purto dolores, eu ius commodo nostrum. Cu eos augue feugait. Vidisse definitiones in pri. Praesent augue arcu, ornare ut tincidunt eu, mattis a libero. Pellentesque habitant morbi tristique senectus et netus et malesuada </p>
                                    </div>
                                </div>
                                <div class="cleared"></div>
                                <div class="cleared"></div>
                            </div>
                            <!-- end widget entry --> </div>
                        <div class="content-column2_1">
                            <div class="widget-entry">
                                <div class="column1">
                                    <div class="gallery ">
                                        <div class="percent_column1 column-last">
                                            <div class="gallery-grid"> <img title="" src="images/team3.jpg"> </div>
                                        </div>
                                        <div class="cleared"></div>
                                    </div>
                                </div>
                                <div class="cleared"></div>
                                <div class="cleared"></div>
                            </div>
                            <!-- end widget entry --> </div>
                        <div class="cleared"></div>
                    </div>
                    <!-- end wrap-column no-wrap -->

                </div>
                <!-- end content section -->
            <?php } ?>

            <?php if ($config['services'] == 1) { ?>
            <div id="services-1" class="content-section black-text white-bg top-spacing-medium bottom-spacing-medium title-spacing-medium align-center " style="">
                <div class="wrap-column">
                    <div class="column1">
                        <div class="wrap">
                            <div class="content-section-heading">
                                <h2 class="content-section-subtitle"> CHOOSE ANY SERVICE FIT YOUR NEED </h2>
                                <h1 class="content-section-title"> SERVICES 1 </h1>
                            </div>
                        </div>
                    </div>
                    <div class="cleared"></div>
                </div>
                <div class="wrap-column">
                    <div class="content-column3_1">
                        <div class="widget-entry">
                            <div class="icon-box">
                                <div class="widget-column1-1 column-last  icon-box-horizontal  ">
                                    <div class="icon-box-item  icon-box-style icon-box-left ">
                                        <div class="icon-box-inline"> <i class="fa fa-eye"></i> DESIGN &amp; UX </div>
                                    </div>
                                </div>
                                <div class="widget-column1-1 column-last  icon-box-horizontal  ">
                                    <div class="icon-box-item  icon-box-style icon-box-left ">
                                        <div class="icon-box-inline"> <i class="fa fa-rocket"></i> WEB DEVELOPMENT </div>
                                    </div>
                                </div>
                                <div class="widget-column1-1 column-last  icon-box-horizontal  widget-element-bottom">
                                    <div class="icon-box-item  icon-box-style icon-box-left widget-element-bottom">
                                        <div class="icon-box-inline"> <i class="fa fa-camera-retro"></i> PHOTOGRAPHY </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end icon box -->
                            <div class="cleared"></div>
                        </div>
                        <!-- end widget entry --> </div>
                    <div class="content-column3_1">
                        <div class="widget-entry">
                            <div class="icon-box">
                                <div class="widget-column1-1 column-last  icon-box-horizontal  ">
                                    <div class="icon-box-item  icon-box-style icon-box-left ">
                                        <div class="icon-box-inline"> <i class="fa fa-bullhorn"></i> MARKETING </div>
                                    </div>
                                </div>
                                <div class="widget-column1-1 column-last  icon-box-horizontal  ">
                                    <div class="icon-box-item  icon-box-style icon-box-left ">
                                        <div class="icon-box-inline"> <i class="fa fa-pencil"></i> CONTENT STRATEGY </div>
                                    </div>
                                </div>
                                <div class="widget-column1-1 column-last  icon-box-horizontal  widget-element-bottom">
                                    <div class="icon-box-item  icon-box-style icon-box-left widget-element-bottom">
                                        <div class="icon-box-inline"> <i class="fa fa-bar-chart-o"></i> RESEARCH &amp; ANALYTICS </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end icon box -->
                            <div class="cleared"></div>
                        </div>
                        <!-- end widget entry --> </div>
                    <div class="content-column3_1">
                        <div class="widget-entry">
                            <div class="icon-box">
                                <div class="widget-column1-1 column-last  icon-box-horizontal  ">
                                    <div class="icon-box-item  icon-box-style icon-box-left ">
                                        <div class="icon-box-inline"> <i class="fa fa-mobile"></i> MOBILE APPS </div>
                                    </div>
                                </div>
                                <div class="widget-column1-1 column-last  icon-box-horizontal  ">
                                    <div class="icon-box-item  icon-box-style icon-box-left ">
                                        <div class="icon-box-inline"> <i class="fa fa-group"></i> SOCIAL NETWORK </div>
                                    </div>
                                </div>
                                <div class="widget-column1-1 column-last  icon-box-horizontal  widget-element-bottom">
                                    <div class="icon-box-item  icon-box-style icon-box-left widget-element-bottom">
                                        <div class="icon-box-inline"> <i class="fa fa-gears"></i> WORDPRESS CUSTOMIZATION </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end icon box -->
                            <div class="cleared"></div>
                        </div>
                        <!-- end widget entry --> </div>
                    <div class="cleared"></div>
                </div>
                <!-- end wrap-column no-wrap -->
            </div>
            <!-- end content section -->
            <?php } ?>

            <?php if ($config['gallery'] == 1) { ?>
                <div id="gallery-1" class="content-section black-text white-bg top-spacing-medium bottom-spacing-medium title-spacing-medium align-center " style="">
                    <div class="wrap-column center">
                        <div class="content-column1">
                            <div class="widget-entry">
                                <div class="column1">
                                    <div class="wrap-column">
                                        <div class="section-widget-heading">
                                            <h3 class="section-widget-title">Gallery 1 - Image Grid</h3>
                                        </div>
                                    </div>
                                    <div class="gallery ">
                                        <div class="percent_column2 ">
                                            <div class="gallery-grid">
                                                <div class="gallery-image-lightbox">
                                                    <div class="gallery-image-bg"></div>
                                                    <img title="Gallery 01" src="images/team3.jpg" alt="1"> <a title="Gallery 01" class="gallery-image-icon" rel="magnific-popup" href="images/promote_box_041.jpg"><i class="pe-7s-search"></i></a> </div>
                                            </div>
                                        </div>
                                        <div class="percent_column2 column-last">
                                            <div class="gallery-grid">
                                                <div class="gallery-image-lightbox">
                                                    <div class="gallery-image-bg"></div>
                                                    <img title="Gallery 02" src="images/team3.jpg" alt="2"> <a title="Gallery 02" class="gallery-image-icon" rel="magnific-popup" href="images/promote_box_02.jpg"><i class="pe-7s-search"></i></a> </div>
                                            </div>
                                        </div>
                                        <div class="cleared"></div>
                                        <div class="percent_column2 ">
                                            <div class="gallery-grid">
                                                <div class="gallery-image-lightbox">
                                                    <div class="gallery-image-bg"></div>
                                                    <img title="Gallery 03" src="images/team3.jpg" alt="3"> <a title="Gallery 03" class="gallery-image-icon" rel="magnific-popup" href="images/promote_box_03.jpg"><i class="pe-7s-search"></i></a> </div>
                                            </div>
                                        </div>
                                        <div class="percent_column2 column-last">
                                            <div class="gallery-grid">
                                                <div class="gallery-image-lightbox">
                                                    <div class="gallery-image-bg"></div>
                                                    <img title="Gallery 04" src="images/team3.jpg" alt="4"> <a title="Gallery 04" class="gallery-image-icon" rel="magnific-popup" href="images/promote_box_01.jpg"><i class="pe-7s-search"></i></a> </div>
                                            </div>
                                        </div>
                                        <div class="cleared"></div>
                                    </div>
                                </div>
                                <div class="cleared"></div>
                                <div class="cleared"></div>
                            </div>
                            <!-- end widget entry --> </div>
                        <div class="cleared"></div>
                    </div>
                    <!-- end wrap-column no-wrap -->

                </div>
                <!-- end content section -->
            <?php } ?>

            <?php if ($config['gallery'] == 2) { ?>
                <div id="gallery-2" class="content-section black-text white-bg top-spacing-no bottom-spacing-medium title-spacing-medium align-center " style="">
                    <div class="wrap-column center">
                        <div class="content-column1">
                            <div class="widget-entry">
                                <div class="column1">
                                    <div class="wrap-no-fullwidth">
                                        <div class="section-widget-heading">
                                            <h3 class="section-widget-title">Gallery 2 - Image Slider</h3>
                                        </div>
                                    </div>
                                    <div class="gallery ">
                                        <div class="flexslider">
                                            <ul class="slides">
                                                <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;" class="">
                                                    <div class="gallery-image-lightbox">
                                                        <div class="gallery-image-bg"></div>
                                                        <img title="Gallery 01" src="images/promote_box_041.jpg" alt="1"> <a title="Gallery 01" class="gallery-image-icon" rel="magnific-popup[pp_gal]" href="images/promote_box_041.jpg"><i class="fa fa-search"></i></a> </div>
                                                </li>
                                                <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: list-item;" class="flex-active-slide">
                                                    <div class="gallery-image-lightbox">
                                                        <div class="gallery-image-bg"></div>
                                                        <img title="Gallery 02" src="images/promote_box_02.jpg" alt="2"> <a title="Gallery 02" class="gallery-image-icon" rel="magnific-popup[pp_gal]" href="images/promote_box_02.jpg"><i class="fa fa-search"></i></a> </div>
                                                </li>
                                                <li style="width: 100%; float: left; margin-right: -100%; position: relative;">
                                                    <div class="gallery-image-lightbox">
                                                        <div class="gallery-image-bg"></div>
                                                        <img title="Gallery 03" src="images/promote_box_03.jpg" alt="3"> <a title="Gallery 03" class="gallery-image-icon" rel="magnific-popup[pp_gal]" href="images/promote_box_03.jpg"><i class="fa fa-search"></i></a> </div>
                                                </li>
                                                <li style="width: 100%; float: left; margin-right: -100%; position: relative;">
                                                    <div class="gallery-image-lightbox">
                                                        <div class="gallery-image-bg"></div>
                                                        <img title="Gallery 04" src="images/promote_box_01.jpg" alt="4"> <a title="Gallery 04" class="gallery-image-icon" rel="magnific-popup[pp_gal]" href="images/promote_box_01.jpg"><i class="fa fa-search"></i></a> </div>
                                                </li>
                                            </ul>
                                            <ul class="flex-direction-nav"><li><a class="flex-prev" href="javascript:;"><i class="fa fa-angle-left"></i></a></li><li><a class="flex-next" href="javascript:;"><i class="fa fa-angle-right"></i></a></li></ul></div>
                                    </div>
                                </div>
                                <div class="cleared"></div>
                                <div class="cleared"></div>
                            </div>
                        </div>
                        <div class="cleared"></div>
                    </div>
                    <!-- end wrap-column no-wrap -->

                </div>
                <!-- end content section -->
            <?php } ?>

            <?php if ($config['gallery'] == 3) { ?>
                <div id="gallery-3" class="content-section black-text white-bg top-spacing-no bottom-spacing-medium title-spacing-medium align-center " style="">
                    <div class="wrap-column center">
                        <div class="content-column1">
                            <div class="widget-entry">
                                <div class="column1">
                                    <div class="wrap-no-fullwidth">
                                        <div class="section-widget-heading">
                                            <h3 class="section-widget-title">Galery 3 - Image Thumb</h3>
                                        </div>
                                    </div>
                                    <div class="gallery ">
                                        <div class="gallery-thumb" data-changing="no" data-width="0">
                                            <div class="gallery-thumb-main">
                                                <div class="gallery-thumb-each gallery-current gallery-main0" data-height="0"  >
                                                    <div class="gallery-image-lightbox">
                                                        <div class="gallery-image-bg"></div>
                                                        <img title="Gallery 01" src="images/promote_box_041.jpg" alt="gallery-01"> <a class="gallery-image-icon" rel="magnific-popup[pp_gal]" href="images/promote_box_041.jpg"><i class="pe-7s-search"></i></a> </div>
                                                </div>
                                                <div class="gallery-thumb-each  gallery-main1" data-height="0"  >
                                                    <div class="gallery-image-lightbox">
                                                        <div class="gallery-image-bg"></div>
                                                        <img title="Gallery 02" src="images/promote_box_02.jpg" alt="gallery-02"> <a class="gallery-image-icon" rel="magnific-popup[pp_gal]" href="images/promote_box_02.jpg"><i class="pe-7s-search"></i></a> </div>
                                                </div>
                                                <div class="gallery-thumb-each  gallery-main2" data-height="0"  >
                                                    <div class="gallery-image-lightbox">
                                                        <div class="gallery-image-bg"></div>
                                                        <img title="Gallery 03" src="images/promote_box_03.jpg" alt="gallery-03"> <a class="gallery-image-icon" rel="magnific-popup[pp_gal]" href="images/promote_box_03.jpg"><i class="pe-7s-search"></i></a> </div>
                                                </div>
                                                <div class="gallery-thumb-each  gallery-main3" data-height="0"  >
                                                    <div class="gallery-image-lightbox">
                                                        <div class="gallery-image-bg"></div>
                                                        <img title="Gallery 04" src="images/promote_box_01.jpg" alt="allery-04"> <a class="gallery-image-icon" rel="magnific-popup[pp_gal]" href="images/promote_box_01.jpg" ><i class="pe-7s-search"></i></a> </div>
                                                </div>
                                            </div>
                                            <div class="gallery-thumb-bottom">
                                                <div class="gallery-thumb-column gallery_percent_column4 "> <img  title="Gallery 01" src="images/promote_box_041.jpg" alt="1">
                                                    <div data-number="0" class="gallery-thumb-border"></div>
                                                </div>
                                                <div class="gallery-thumb-column gallery_percent_column4 "> <img  title="Gallery 02" src="images/promote_box_02.jpg" alt="2">
                                                    <div data-number="1" class="gallery-thumb-border"></div>
                                                </div>
                                                <div class="gallery-thumb-column gallery_percent_column4 "> <img  title="Gallery 03" src="images/promote_box_03.jpg" alt="3">
                                                    <div data-number="2" class="gallery-thumb-border"></div>
                                                </div>
                                                <div class="gallery-thumb-column gallery_percent_column4 column-last"> <img  title="Gallery 04" src="images/promote_box_01.jpg" alt="4">
                                                    <div data-number="3" class="gallery-thumb-border"></div>
                                                </div>
                                                <div class="cleared"></div>
                                                <div class="cleared"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cleared"></div>
                                <div class="cleared"></div>
                            </div>
                            <!-- end widget entry --> </div>
                        <div class="cleared"></div>
                    </div>
                    <!-- end wrap-column no-wrap -->

                </div>
                <!-- end content section -->
            <?php } ?>

            <?php if ($config['testimonials'] == 1) { ?>
            <div id="testimonials-1" class="content-section black-text white-bg top-spacing-medium bottom-spacing-big title-spacing-medium align-center " style="">
                <div class="wrap-column">
                    <div class="column1">
                        <div class="wrap">
                            <div class="content-section-heading">
                                <h1 class="content-section-title"> TESTIMONIALS 1 </h1>
                            </div>
                        </div>
                    </div>
                    <div class="cleared"></div>
                </div>
                <div class="wrap-column center">
                    <div class="content-column1">
                        <div class="widget-entry">
                            <div class="column1"> </div>
                            <div class="cleared"></div>
                            <div class="column1">
                                <div class="testimonials-slider testimonials-slider-box">
                                    <div class="testimonials-container">
                                        <div class="testimonials-each testimonials-current">
                                            <div class="testimonials-content">
                                                <p>One of the easiest premium wordpress themes to hit the ground running on. It&#8217;s not overlay complicated, has more than enough customizations and great documentation</p>
                                            </div>
                                            <div class="testimonials-author">
                                                <div class="testimonials-image">
                                                    <div class="testimonials-image-box"><img alt="JOHN DOE" src="images/client_01.png"></div>
                                                </div>
                                                <div class="testimonials-author-detail">
                                                    <div class="testimonials-author-name">HEADING</div>
                                                    <div class="testimonials-author-info content">
                                                        <p>SUB-HEADING</p>
                                                    </div>
                                                    <div class="cleared"></div>
                                                </div>
                                            </div>
                                            <div class="testimonials-id">1</div>
                                        </div>
                                        <div class="testimonials-each ">
                                            <div class="testimonials-content">
                                                <p>One of the easiest premium wordpress themes to hit the ground running on. It&#8217;s not overlay complicated, has more than enough customizations and great documentation</p>
                                            </div>
                                            <div class="testimonials-author">
                                                <div class="testimonials-image">
                                                    <div class="testimonials-image-box"><img alt="ANTHONY WILLIAMS " src="images/client_03.jpg"></div>
                                                </div>
                                                <div class="testimonials-author-detail">
                                                    <div class="testimonials-author-name">HEADING</div>
                                                    <div class="testimonials-author-info content">
                                                        <p>SUB-HEADING</p>
                                                    </div>
                                                    <div class="cleared"></div>
                                                </div>
                                            </div>
                                            <div class="testimonials-id">2</div>
                                        </div>
                                        <div class="testimonials-each ">
                                            <div class="testimonials-content">
                                                <p>One of the easiest premium wordpress themes to hit the ground running on. It&#8217;s not overlay complicated, has more than enough customizations and great documentation</p>
                                            </div>
                                            <div class="testimonials-author">
                                                <div class="testimonials-image">
                                                    <div class="testimonials-image-box"><img alt="MARY CAMERON " src="images/client_02.jpg"></div>
                                                </div>
                                                <div class="testimonials-author-detail">
                                                    <div class="testimonials-author-name">HEADING</div>
                                                    <div class="testimonials-author-info content">
                                                        <p>SUB-HEADING</p>
                                                    </div>
                                                    <div class="cleared"></div>
                                                </div>
                                            </div>
                                            <div class="testimonials-id">3</div>
                                        </div>
                                    </div>
                                    <div class="testimonials-buttons">
                                        <div class="testimonials-button-item active" data-id="1"> </div>
                                        <div class="testimonials-button-item " data-id="2"> </div>
                                        <div class="testimonials-button-item " data-id="3"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cleared"></div>
                        </div>
                        <!-- end widget entry --> </div>
                    <div class="cleared"></div>
                </div>
                <!-- end wrap-column no-wrap -->

            </div>
            <!-- end content section -->
            <?php } ?>

            <?php if ($config['testimonials'] == 2) { ?>
            <div id="testimonials-2" class="content-section black-text gray-bg top-spacing-big bottom-spacing-medium title-spacing-medium align-center " style="">
                <div class="wrap-column">
                    <div class="column1">
                        <div class="wrap">
                            <div class="content-section-heading">
                                <h1 class="content-section-title"> TESTIMONIALS 2 </h1>
                            </div>
                        </div>
                    </div>
                    <div class="cleared"></div>
                </div>
                <div class="wrap-column">
                    <div class="content-column1">
                        <div class="widget-entry">
                            <div class="column1"> </div>
                            <div class="cleared"></div>
                            <div class="column1">
                                <div class="testimonials-slider testimonials-slider-section">
                                    <div class="testimonials-container">
                                        <div class="testimonials-each testimonials-current">
                                            <div class="testimonials-content">
                                                <p>One of the easiest premium wordpress themes to hit the ground running on. It&#8217;s not overlay complicated, has more than enough customizations and great documentation</p>

                                            </div>
                                            <div class="testimonials-author">
                                                <div class="testimonials-image">
                                                    <div class="testimonials-image-box"><img alt="JOHN DOE" src="images/client_01.png"></div>
                                                </div>
                                                <div class="testimonials-author-detail">
                                                    <div class="testimonials-author-name">HEADING</div>
                                                    <div class="testimonials-author-info content">
                                                        <p>SUB-HEADING</p>
                                                    </div>
                                                    <div class="cleared"></div>
                                                </div>
                                            </div>
                                            <div class="testimonials-id">1</div>
                                        </div>
                                        <div class="testimonials-each ">
                                            <div class="testimonials-content">
                                                <p>One of the easiest premium wordpress themes to hit the ground running on. It&#8217;s not overlay complicated, has more than enough customizations and great documentation</p>
                                            </div>
                                            <div class="testimonials-author">
                                                <div class="testimonials-image">
                                                    <div class="testimonials-image-box"><img alt="ANTHONY WILLIAMS " src="images/client_03.jpg"></div>
                                                </div>
                                                <div class="testimonials-author-detail">
                                                    <div class="testimonials-author-name">HEADING</div>
                                                    <div class="testimonials-author-info content">
                                                        <p>SUB-HEADING</p>
                                                    </div>
                                                    <div class="cleared"></div>
                                                </div>
                                            </div>
                                            <div class="testimonials-id">2</div>
                                        </div>
                                        <div class="testimonials-each ">
                                            <div class="testimonials-content">
                                                <p>One of the easiest premium wordpress themes to hit the ground running on. It&#8217;s not overlay complicated, has more than enough customizations and great documentation</p>
                                            </div>
                                            <div class="testimonials-author">
                                                <div class="testimonials-image">
                                                    <div class="testimonials-image-box"><img alt="MARY CAMERON " src="images/client_02.jpg"></div>
                                                </div>
                                                <div class="testimonials-author-detail">
                                                    <div class="testimonials-author-name">HEADING</div>
                                                    <div class="testimonials-author-info content">
                                                        <p>SUB-HEADING</p>
                                                    </div>
                                                    <div class="cleared"></div>
                                                </div>
                                            </div>
                                            <div class="testimonials-id">3</div>
                                        </div>
                                    </div>
                                    <div class="testimonials-buttons">
                                        <div class="testimonials-button-item active" data-id="1"> </div>
                                        <div class="testimonials-button-item " data-id="2"> </div>
                                        <div class="testimonials-button-item " data-id="3"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cleared"></div>
                        </div>
                        <!-- end widget entry --> </div>
                    <div class="cleared"></div>
                </div>
                <!-- end wrap-column no-wrap -->

            </div>
            <!-- end content section -->
            <?php } ?>

        </div>
        <div id="footer">
            <div class="wrap-column">
                <div id="footer-widget-container">
                    <div class="footer-column">
                        <div id="text-2" class="footer-widget widget_text content">
                            <div class="footer-widget-title">ABOUT US</div>
                            <div class="textwidget">Megaw is elegant business wordpress theme built for many purpose. You can use this premium wordpress themes for any kind of website.
                            </div>
                        </div>
                    </div>
                    <div class="footer-column">
                        <div id="wope_multi_line_menu_widget-2" class="footer-widget widget_wope_multi_line_menu_widget content">
                            <div class="footer-widget-title">Quick Links</div>
                            <div class='multi_line_menu_container_2'>
                                <div class="menu-footer-widget-menu-1-container">
                                    <ul id="menu-footer-widget-menu-1" class="menu">
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3"><a href="#">Facebook</a></li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4"><a href="#">Twitter</a></li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5"><a href="#">Instagram</a></li>
                                    </ul>
                                </div>
                                <div class="cleared"></div>
                            </div>
                        </div>
                    </div>
                    <div class="footer-column">
                        <div id="text-3" class="footer-widget widget_text content">
                            <div class="footer-widget-title">Contact Us</div>
                            <div class="textwidget">Office: 12345 Little Lonsdale St, Melbourne<br>
                                Phone: (123) 123-456<br>
                                Fax: (123) 123-456<br>
                                E-Mail: johndoe@site.com<br>
                                Web: http://site.com</div>
                        </div>
                    </div>
                    <div class="cleared"></div>
                </div>
                <!-- End Footer Widget Container-->
            </div>
        </div>
        <!-- End Footer -->
        <div id="footer-bottom" >
            <div class="wrap">
                <div class="footer-copyright"> Copyright 2015 Megaw - Designed by <a title="Premium Template Theme" href="">Company</a> </div>
            </div>
        </div>
        <!-- End Footer Bottom -->
    </div>
    <!-- End Page -->
</div>
<!-- End Site Background -->

<div id="yith-quick-view-modal">
    <div class="yith-quick-view-overlay"></div>
    <div class="yith-wcqv-wrapper">
        <div class="yith-wcqv-main">
            <div class="yith-wcqv-head"> <a href="#" id="yith-quick-view-close" class="yith-wcqv-close">X</a> </div>
            <div id="yith-quick-view-content" class="woocommerce single-product"></div>
        </div>
    </div>
</div>
<script src='js/jquery/ui/core.min.js'></script>
<script src='js/jquery/ui/widget.min.js'></script>
<script src='js/jquery/ui/accordion.min.js'></script>
<script src='js/script.js'></script>
<script src='js/jquery.isotope.min.js'></script>
<script>
    /* <![CDATA[ */
    var megamenu = {"effect":{"fade":{"in":{"animate":{"opacity":"show"}},"out":{"animate":{"opacity":"hide"}}},"slide":{"in":{"animate":{"height":"show"},"css":{"display":"none"}},"out":{"animate":{"height":"hide"}}}},"fade_speed":"fast","slide_speed":"fast","timeout":"300"};
    /* ]]> */
</script>
</body>
</html>